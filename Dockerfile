# CentOS 6 with ssh + custom script to setup passwordless SSH from ClusterControl container.

FROM centos:6
MAINTAINER Severalnines <info@severalnines.com>

RUN yum -y update && \
	yum -y install openssh-server openssh-clients passwd curl && \
	yum clean all && \
	rpm -Uvh http://download.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm && \
	yum update && \
	echo "fs.nr_open=2000000" >> /etc/sysctl.conf

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 22 9999 3306 27107 5432
